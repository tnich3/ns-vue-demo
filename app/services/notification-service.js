/** @module NotificationService */

import { LocalNotifications } from "nativescript-local-notifications";

/** Service providing local notification methods. */
export const NotificationService = {
    /**
     * Checks if we have permission to schedule a local notification.
     * Android does not need granted permission to schedule a notification and will always return true.
     * iOS does need permission to be explicitly granted.
     * @return {Promise} A promise whos input is a boolean indicating whether we have permission or not.
     */
    hasPermission: function () {
        return LocalNotifications.hasPermission();
    },
    /**
     * Creates a local notification.
     * Currently all options are hard coded. Add parameters as we nail down what is going to vary for our notifications.
     * @return {Promise} A promise
     */
    createNotification: function () {
        //TODO Continue refactoring this so we get targeted notification creations needed by the app
        return LocalNotifications.schedule(
            [{
                id: 1,
                title: "Specimen Collection Reminder",
                body: "It's time to collect your next specimen.",
                at: new Date(new Date().getTime() + (5 * 1000)), // 5 seconds from now
                actions: [
                    {
                        id: "collected",
                        type: "button",
                        title: "Collected",
                        launch: false
                    },
                    {
                        id: "snooze",
                        type: "button",
                        title: "Snooze",
                        launch: false
                    }
                ]
            }]
        );
    }
};
