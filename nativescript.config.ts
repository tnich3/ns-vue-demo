import { NativeScriptConfig } from '@nativescript/core';

export default {
  id: 'com.westat.ns.vue.helloworld',
  appResourcesPath: 'App_Resources',
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'none'
  }
} as NativeScriptConfig;