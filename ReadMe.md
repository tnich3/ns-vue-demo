# NativeScript HelloWorld Vue App

## Environment Setup

Setup instructions <https://docs.nativescript.org/start/quick-setup#quick-setup>

I ran this script to install the bulk of the components.

```shell
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://www.nativescript.org/setup/win'))"
```

Everything executes correctly. I use the `tns doctor` check afterwards and it says everything is OK. But there are no emulators setup to debug the project. I also do not see Android Studio installed. I'm not sure if Android Studio is necessary to create and manage the Android emulators.

Install Android Studio separately. <https://developer.android.com/studio>
Install Android Studio and the Virtual Emulator option.

## Connect Devices

The easiest thing is to connect your physical phone for debugging.

### Preview in PlayGround App

[Step 3](https://docs.nativescript.org/start/quick-setup#step-3-install-the-nativescript-playground-app) of the NativeScript CLI setup is to install the NativeScript playground app on your phone.

Install the app start it up. In your terminal run `tns preview`. Part of the output will be a QR code. Scan this with your playground app and your app with be synched to your phone. This includes live code updates.

### Connect an Android Device for Debugging

- Plug your Android device in to the computer.
- Open up Settings
  - Go to About
  - Go to Software information, or whatever screen has the build number
- Click on the build number 7 times to enable developer mode
- Go back to the main settings screen and all the way to the bottom where there is now Developer Options
- Turn usb debugging on
- Click to trust the computer

Now you can run `tns device android` and you should see your device listed here.

Run `tns debug android` to

### Connect an Emulator for Debugging

I couldn't get this going.

Running Intel® HAXM installer
Intel® HAXM installation failed. To install Intel® HAXM follow the instructions found at: https://software.intel.com/android/articles/installation-instructions-for-intel-hardware-accelerated-execution-manager-windows

Check for available devices for Android

```shell
tns device android
```

NativeScript [docs](https://docs.nativescript.org/tooling/android-virtual-devices) are saying to use the `avdmanager` cli to view emulator instances. But on my system this is not a thing that does anything. The emulator command does work, though.

```shell
cd $ANDROID_HOME/tools/bin
emulator -list-avds
```

This shows me `Pixel_3a_API_30_x86` which I believe was installed with Android Studio. All your created emulators should be stored in `C:\Users\$user\.android\avd\`

So this may be because I don't have the hardware accelaration setup? Looking at installing [WHPX](https://developer.android.com/studio/run/emulator-acceleration#vm-windows-whpx).

- Open up Turn Windows Features On or Off
- Check off Windows HyperVisor Platform
- Click OK
- Restart

### Interactive Debugging

Install the [NativeScript extension](https://marketplace.visualstudio.com/items?itemName=NativeScript.nativescript) for VS Code.

Open up a code file that can be debugged. Then open up the debug view of VS Code. If the project does not have a launch.json file it will prompt you to create one. You want to create one using the NativeScript environment type.

With the NativeScript configuration added to your launch.json you will have several dropdown options next to the run button. If you have devices configured for your chosen platform pick `Launch Android` and click run. This will launch the device, sync your app to it, and attach the debugger. This debug mode includes live syncing of your code.

Here is a video laying out how to do this. <https://www.youtube.com/watch?v=1e4eFkPxQSw>

## Notes

One important note is the views we are composing in NativeScript do not use html elements. We need to use NAtiveScript components. Documentation for NativeScript components can be found [here](https://docs.nativescript.org/ui/components/action-bar).

It looks like the event binding for NativeScript components is different than the JavaScript framework syntax you are working with. For example, attaching a button click event in Vue looks like this:

```xml
<Button :text="buttonText" @tap="on_click()"></Button>
```

Notice the event name itself is different than you're used to. Supported events can be found in the component documentation. Also notice the syntax is to use the `@` symbol to bind the event handler.

### Local Notifications

These are the push notifications we want to use. Push notifications are the alerts that come from a remote server. Local notifications are the same thing but originate from the local app.

NativeScript has a plugin to use local notifications [here](https://market.nativescript.org/plugins/nativescript-local-notifications/).
